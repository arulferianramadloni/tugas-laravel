@extends('layout.master')

@section('judul')
    Halaman Home
@endsection
@section('content')
    <h5>Media Online</h5>
    <h5>Sosial Media Developer</h5>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h5>Benefit Join di Media Online</h5>
    <ul>
        <li>Mendapatkan motivasi dari sesama Developer</li>
        <li>Sharing knowledge</li>
        <li>Dibuat oleh calon developer terbaik</li>
    </ul>
    <h5>Cara Bergabung ke Media Online</h5>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di link <a href="/register">Form Sign Up</a> </li>
        <li>Selesai</li>
@endsection
    
